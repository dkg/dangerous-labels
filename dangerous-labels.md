---
title: "Dangerous Labels in DNS and E-mail"
docname: draft-dkg-intarea-dangerous-labels-05
category: info

ipr: trust200902
area: sec
workgroup: intarea
keyword: Internet-Draft
submissionType: IETF
venue:
  group: "Internet Area"
  type: "Working Group"
  mail: "intarea@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/intarea/"
  repo: "https://gitlab.com/dkg/dangerous-labels"
  latest: "https://dkg.gitlab.io/dangerous-labels/"

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
 -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    country: USA
    abbrev: ACLU
    email: dkg@fifthhorseman.net
informative:
 AUTOCONF:
   target: https://roll.urown.net/server/mail/autoconfig.html
   title: Mail Client Auto-Configuration
   date: 2021-02-22
   author:
     name: Alain Wolf
 THUNDERBIRD:
   target: https://github.com/mdn/archived-content/tree/main/files/en-us/mozilla/thunderbird/autoconfiguration
   title: Autoconfiguration in Thunderbird
   author:
     name: Ben Bucksch
   date: 2021-05-03
 AUTODISCOVER:
   target: https://docs.microsoft.com/en-us/exchange/client-developer/exchange-web-services/autodiscover-for-exchange
   title: Autodiscover for Exchange
   date: 2020-01-15
   author:
     org: Microsoft
 CABForum-BR:
    target: https://cabforum.org/wp-content/uploads/CA-Browser-Forum-BR-1.8.4.pdf
    title: CA/Browser Forum Baseline Requirements
    date: 2022-04-23
    author:
      org: CA/Browser Forum
 VU591120:
   target: https://www.kb.cert.org/vuls/id/591120/
   title: Multiple SSL certificate authorities use predefined email addresses as proof of domain ownership
   date: 2015-04-07
   author:
     org: CERT Coordination Center
 UTR36:
   target: https://unicode.org/reports/tr36/
   title: Unicode Security Considerations
   author:
    - 
      name: Mark Davis
    -
      name: Michel Suignard
 OPENPGP-CA:
   target: https://openpgp-ca.org/background/details/
   title: "OpenPGP CA: Technical Details"
   author:
     org: the OpenPGP CA project
 SSH-OPENPGP-AUTH:
   target: https://codeberg.org/wiktor/ssh-openpgp-auth#technical-details
   title: "SSH OpenPGP Authenticator: Technical details"
   author:
     name: Wiktor Kwapisiewicz
normative:
--- abstract

This document establishes registries that list known security-sensitive labels in the DNS and in e-mail contexts.

It provides references and brief explanations about the risks associated with each known label.

The registries established here offer guidance to the security-minded system administrator, who may not want to permit registration of these labels by untrusted users.

--- middle

# Introduction

The Internet has a few places where seemingly arbitrary labels can show up and be used interchangeably.

Some choices for those labels have surprising or tricky consequences.
Reasonable admnistrators may want to be aware of those labels to avoid an accidental allocation that has security implications.

This document registers a list of labels in DNS and e-mail systems that are known to have a security impact.
It is not recommended to create more security-sensitive labels.

Offering a stable registry of these dangerous labels is not an endorsement of the practice of using arbitrary labels in this way.
A new protocol that proposes adding a label to this list is encouraged to find a different solution if at all possible.

## Requirements Language

{::boilerplate bcp14-tagged}

# DNS Labels {#dns}

Note that {{!RFC8552}} defines the use of "underscored" labels which are treated differently than normal DNS labels, and often have security implications.
That document also established the IANA registry for "Underscored and Globally Scoped DNS Node Names".
That registry takes precedence to the list enumerated here, and any label in that list or with a leading underscore ("`_`") MUST NOT be included in this list.

Note also that {{Section 2.2 of ?RFC8820}} makes it clear that depending on specific forms of DNS labels in a given URI scheme in a protocol is strongly discouraged.

Below are some normal-looking DNS labels that may grant some form of administrative control over the domain that they are attached to.

They are mostly "leftmost" or least-significant labels (in the sense used in {{Section 8 of ?RFC8499}}), in that if `foo` were listed here, it would be because granting control over the `foo.example.net` label (or control over the host pointed to by `foo.example.net`) to an untrusted party might offer that party some form of administrative control over other parts of `example.org`.

Note: where "\<key-tag\>" occurs in {{dns-table}}, it indicates any sequence of five or more decimal digits, as described in {{?RFC8509}}.

{: title="Dangerous DNS labels" #dns-table}
DNS Label | Rationale | Reference
---|------------------|-----
`autoconfig` | Hijack mail user agent autoconfiguration | {{AUTOCONF}}, {{THUNDERBIRD}}
`autodiscover` | Hijack Microsoft Exchange client configuration | {{AUTODISCOVER}}
`imap` | Hijack mail user agent autoconfiguration | {{AUTOCONF}}, {{THUNDERBIRD}}
`imaps` | Hijack mail user agent autoconfiguration | {{AUTOCONF}}
`mail` | Hijack mail user agent autoconfiguration | {{AUTOCONF}}, {{THUNDERBIRD}}
`mta-sts` | Set SMTP transport security policy | {{?RFC8461}}
`openpgpkey` | Domain-based OpenPGP certificate lookup and verification | {{?I-D.koch-openpgp-webkey-service}}
`pop3` | Hijack mail user agent autoconfiguration | {{AUTOCONF}}
`pop3s` | Hijack mail user agent autoconfiguration | {{AUTOCONF}}
`root-key-sentinel-is-ta-`\<key-tag\> | Indicates which DNSSEC root key is trusted | {{RFC8509}}
`root-key-sentinel-not-ta-`\<key-tag\> | Indicates which DNSSEC root key is not trusted | {{RFC8509}}
`smtp` | Hijack mail user agent autoconfiguration | {{AUTOCONF}}, {{THUNDERBIRD}}
`smtps` | Hijack mail user agent autoconfiguration | {{AUTOCONF}}
`submission` | Hijack mail user agent autoconfiguration | {{AUTOCONF}}
`wpad` | Automatic proxy discovery | {{?I-D.ietf-wrec-wpad-01}}
`www` | Popular web browsers guess this label | FIXME: find a reference


# E-mail Local Parts {#e-mail}

{{Section 3.4.1 of !RFC5322}} defines the `local-part` of an e-mail address (the part before the "`@`" sign) as "domain-dependent".
However, allocating some specific `local-part`s to an untrusted party can have significant security consequences for the domain or other associated resources.

Note that all these labels are expected to be case-insensitive.
That is, an administrator restricting registration of a `local-part` named "`admin`" MUST also apply the same constraint to "`Admin`" or "`ADMIN`" or "`aDmIn`".

{{?RFC2142}} offers some widespread historical practice for common `local-part`s.
The CA/Browser Forum's Baseline Requirements ({{CABForum-BR}}) constrain how any popular Public Key Infrastructure (PKI) Certification Authority (CA) should confirm domain ownership when verifying by e-mail.
The public CAs used by popular web browsers ("web PKI") will adhere to these guidelines, but anyone relying on unusual CAs may still be subject to risk additional labels described here.

{: title="Dangerous E-mail local-parts" #e-mail-table}
E-mail local-part | Rationale | References
---|------------------|----
`abuse` | Receive reports of abusive public behavior | {{Section 2 of RFC2142}}
`administrator` | PKI Cert Issuance | Section 3.2.2.4.4 of {{CABForum-BR}}
`admin` | PKI Cert Issuance | Section 3.2.2.4.4 of {{CABForum-BR}}
`hostmaster` | PKI Cert Issuance, DNS zone control | Section 3.2.2.4.4 of {{CABForum-BR}}, {{Section 7 of RFC2142}}
`info` | PKI Cert Issuance (historical) | {{VU591120}}
`is` | PKI Cert Issuance (historical) | {{VU591120}}
`it` | PKI Cert Issuance (historical) | {{VU591120}}
`noc` | Receive reports of network problems | {{Section 4 of RFC2142}}
`openpgp-ca` | Make authoritative-seeming OpenPGP certifications for in-domain e-mail addresses | {{OPENPGP-CA}}
`postmaster` | Receive reports related to SMTP service, PKI Cert Issuance | {{Section 5 of RFC2142}}, Section 3.2.2.4.4 of {{CABForum-BR}}
`root` | Receive system software notifications, PKI Cert Issuance (historic) | {{VU591120}}, FIXME: find a reference for `root` (software config docs?)
`security` | Receive reports of technical vulnerabilities | {{Section 4 of RFC2142}}
`ssh-openpgp-auth` | Trust anchor for SSH host keys | {{SSH-OPENPGP-AUTH}}
`ssladministrator` | PKI Cert Issuance (historical) | {{VU591120}}
`ssladmin` | PKI Cert Issuance (historical) | {{VU591120}}
`sslwebmaster` | PKI Cert Issuance (historical) | {{VU591120}}
`sysadmin` | PKI Cert Issuance (historical) | {{VU591120}}
`webmaster` | PKI Cert Issuance, Receive reports related to HTTP service | Section 3.2.2.4.4 of {{CABForum-BR}}, {{Section 5 of RFC2142}}
`www` | Common alias for `webmaster` | {{Section 5 of RFC2142}}

# Security Considerations

Allowing untrusted parties to allocate names with the labels associated in this document may grant access to administrative capabilities.

The administrator of a DNS or E-mail service that permits any untrusted party to register an arbitrary DNS label or e-mail `local-part` for their own use SHOULD reject attempts to register the labels listed here.

## Additional Risks Out of Scope

The lists of security concerns in this document cover security risks and concerns associated with interoperable use of specific labels.
They do not cover every possible security concern associated with any DNS label or e-mail localpart.

For example, DNS labels with an existing underscore are likely by construction to be sensitive, and are registered separately in the registry defined by {{RFC8552}}.

Similarly, where humans or other systems capable of transcription error are in the loop, subtle variations of the labels listed here may also have security implications, due to homomgraphic confusion ({{?Homograph=DOI.10.1145/503124.503156}}), but this document does not attempt to enumerate all phishing, typosquatting, or similar risks of visual confusion, nor does it exhaustively list all other potential risks associated with variant encodings.
See {{UTR36}} for a deeper understanding of these categories of security concerns.

Additionally, some labels may be associated with security concerns that happen to also commonly show up as DNS labels or e-mail local parts, but their risk is not associated with their use in interoperable public forms of DNS or e-mail.
For example, on some systems, a local user account named `backup` has full read access to the local filesystem so that it can transfer data to the local backup system.
And in some cases, the list of local user accounts is also aliased into e-mail local parts.
However, permitting the registration of `backup@example.net` as an e-mail address is not itself an interoperable security risk -- no external third party will treat any part of the `example.net` domain differently because of the registration.
This document does not cover any risk entirely related to internal configuration choices.

# IANA Considerations

This document asks IANA to establish two registries, from {{dns-table}} and {{e-mail-table}}.

## Dangerous DNS Labels Registry

The table of Dangerous DNS Labels (in {{dns-table}}) has three columns:

- DNS Label (text string)
- Rationale (human-readable short explanation)
- References (pointer or pointers to more detailed documentation)

Note that this table does not include anything that should be handled by the pre-existing "Underscored and Globally Scoped DNS Node Names" registry defined by {{RFC8552}}.

Following the guidance in {{!BCP26}}, any new entry to this registry will be assigned using Specification Required.
The IESG will assign one or more designated experts for this purpose, who will consult with the IETF DNSOP working group mailing list or its designated successor.
The Designated Expert will support IANA by clearly indicating when a new DNS label should be added to this table, offering the label itself, a brief rationale, and a pointer to the permanent and readily available documentation of the security consequences of the label.
Updates or deletions of DNS Labels will follow the same process.

## Dangerous E-mail Local Parts Registry

The table of Dangerous E-mail Local Parts (in {{e-mail-table}} also has three columns:

- E-mail local part (text string)
- Rationale (human-readable short explanation)
- References (pointer or ponters to more detailed documentation)

Following the guidance in {{BCP26}}, any new entry to this registry will be assigned using Specification Required.
The IESG will assign one or more designated experts for this purpose, who will consult with the IETF EMAILCORE working group mailing list or its designated successor.
The Designated Expert will support IANA by clearly indicating when a new e-mail local part should be added to this table, offering the local part itself, a brief rationale, and a pointer to the permanent and readily available documentation of the security consequences of the local part.
Updates or deletions of of E-mail Local Parts will follow the same process.

## Shared Considerations

Having to add a new security-sensitive entry to either of these tables is likely to be a bad idea, because existing DNS zones and e-mail installations may have already made an allocation of the novel label, and cannot avoid the security implications.
For a new protocol that wants to include a label in either registry, there is almost always a better protocol design choice.

Yet, if some common practice permits any form of administrative control over a separate resource based on control over an arbitrary label, administrators need a central place to keep track of which labels are dangerous.

If such a practice cannot be avoided, it is better to ensure that the risk is documented clearly and referenced in the appropriate registry, rather than leaving it up to each administrator to re-discover the problem.

--- back

# Acknowledgements

Many people created these dangerous labels or the authorization processes that rely on them over the years.

Dave Crocker wrote an early list of special e-mail local-parts, from {{RFC2142}}.

Paul Hoffman tried to document a wider survey of special DNS labels (not all security-sensitive) in {{?I-D.hoffman-dns-special-labels}}.

Rasmus Dahlberg, yuki, and Carsten Bormann reviewed this draft and gave feedback.

Tim Wicinski pointed out wpad.

{:removeinrfc}
# Document Considerations

## Other types of labels?

This document is limited to leftmost DNS labels and e-mail local-parts because those are the arbitrary labels that the author is familiar with.
There may be other types of arbitrary labels on the Internet with special values that have security implications that the author is not aware of.
If you are aware of some other system with a similar pattern, please send feedback.

## Document History

### Substantive Changes from -03 to -04

- Add `ssh-openpgp-auth` e-mail local part

### Substantive Changes from -02 to -03

- Add `mail` DNS label (MUA autoconfiguration)
- Reference Thunderbird guidance about MUA autoconfiguration
- Add `openpgp-ca` e-mail local part

### Substantive Changes from -01 to -02

- New dangerous DNS labels: WPAD, MS Exchange Autodiscover, common MUA
  autoconfig (originally from Mozilla Thunderbird)

### Substantive Changes from -00 to -01

- explicitly define IANA tables
- indicate that the tables use Specification Required
- clarify scope
