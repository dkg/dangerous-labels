#!/usr/bin/make -f
#
# Prerequisites: apt install ruby-kramdown-rfc2629 xml2rfc
#  (for pdf build, also weasyprint)

draft = dangerous-labels
OUTPUT = $(draft).txt $(draft).html $(draft).xml

all: $(OUTPUT)

%.xml: %.md
	kramdown-rfc < $< > $@.tmp
	mv $@.tmp $@

%.html: %.xml
	xml2rfc $< --html

%.txt: %.xml
	xml2rfc $< --text

%.pdf: %.xml
	xml2rfc $< --pdf

clean:
	-rm -rf $(OUTPUT) $(draft).pdf

check:
	codespell $(draft).md README.md

.PHONY: clean all check
